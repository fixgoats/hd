import numpy as np
import scipy.linalg as la
import matplotlib.pyplot as plt
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

A=100
B=50
oms= 49
M = 9
# Dimension of the total Hilbert space
total_dim = 2**(M+1)
dk = 2*np.pi*M**(-1)
k = np.arange(-M/2,M/2+1)*dk
annih = np.array([[0,1],[0,0]])
creation = np.array([[0,0],[1,0]])
dt = 0.01
omk = A + B*np.cos(k)
n = np.array([[0,0],[0,1]])
HP = np.zeros((total_dim,total_dim))
for i in range(M):
    h0 = np.eye(2)
    for j in range(M):
        if i == j:
            b0 = n
        else:
            b0 = np.eye(2)
        h0 = np.kron(h0,b0)
    HP = HP + omk[i]*h0

I0 = np.zeros((total_dim,total_dim))
I1 = np.zeros((total_dim,total_dim))
for i in range(M):
    h0 = creation
    h1 = annih
    for j in range(M):
        if i == j:
            b0 = annih
            b1 = creation
        else:
            b0 = np.eye(2)
            b1 = np.eye(2)
        h0 = np.kron(h0,b0)
        h1 = np.kron(h1,b1)
    I0 = I0 + h0
    I1 = I1 + h1
HI = I0 + I1

N = n #Observable we'll take the mean value of
for i in range(M):
    N = np.kron(N,np.eye(2))
HA = oms*N

H = HP + HI + HA
#w, v = la.eig(H)
#DH = np.diag(w)
#print(np.matmul(v.T,v.conj()))
state = np.array([0,1])
for i in range(M):
    state = np.kron(state,np.array([1,0]))
nt = np.ones(10000)
#dUt = np.matmul(v.T,np.matmul(la.expm(-1j*dt*DH),v.conj()))
dUt = la.expm(-1j*dt*H)
for i in range(1,10000):
    state = np.matmul(dUt,state)
    nt[i] = np.matmul(state.conj(),np.matmul(N,state))
fig, ax = plt.subplots()
t = np.arange(10000)*dt
ax.plot(t, nt)
ax.set(xlabel="$t$", ylabel = "$n_a(t)$")
ax.grid()

fig.savefig("exact.png")
