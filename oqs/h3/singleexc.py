import numpy as np
import scipy.linalg as la
import matplotlib.pyplot as plt
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

A=100
B=50
oms= 49
M = 1000
dk = 2*np.pi*M**(-1)
k = np.arange(-M/2,M/2+1)*dk
b = np.array([[0,1],[0,0]])
bc = np.array([[0,0],[1,0]])
dt = 0.001
omk = A + B*np.cos(-np.pi + k)
# I'm attempting to avoid working with >2^1000 dimensional matrices (kroneckering
# together a 1000 ladder operators), but this approach doesn't seem to work
hmat0 = np.zeros((M+1,M+1))
for i in range(1,M+1):
    hmat0[i,i] = omk[i-1]
hmat1 = np.zeros((M+1,M+1))
for i in range(1,M+1):
    hmat1[i,0] = 1
    hmat1[0,i] = 1
hmat2 = oms*np.zeros((M+1,M+1))
hmat2[0,0] = oms
hmat = hmat0 + hmat1 + hmat2
#w, v = la.eigh(hmat)
#print(np.matmul(v.T,v.conj()))
#eighmat = np.diag(w)
state = np.zeros(M+1)
ReAt = np.ones(1000)
ImAt = np.zeros(1000)
state[0] = 1
nt = np.ones(1000)
dUt = la.expm(-1j*dt*hmat)
for i in range(1,1000):
    state = np.matmul(dUt,state)
    ReAt[i] = state[i].real
    ImAt[i] = state[i].imag
    nt[i] = abs(state[0])**2
fig, ax = plt.subplots()
t = np.arange(1000)*dt
ax.plot(t, nt)
ax.set(xlabel="$t$", ylabel = "$n_a(t)$")
ax.grid()

fig.savefig("singleexc49.png")

fig, ax = plt.subplots()
ax.plot(t, ReAt)
ax.set(xlabel="$t$", ylabel = "$\\Re(A(t))$")
ax.grid()

fig.savefig("reat.png")

fig, ax = plt.subplots()
ax.plot(t, ImAt)
ax.set(xlabel="$t$", ylabel = "$\\Im(A(t))$")
ax.grid()

fig.savefig("imat.png")
