\begin{Verbatim}[commandchars=\\\{\}]
\PYG{k+kn}{import} \PYG{n+nn}{numpy} \PYG{k}{as} \PYG{n+nn}{np}
\PYG{k+kn}{import} \PYG{n+nn}{scipy.linalg} \PYG{k}{as} \PYG{n+nn}{la}
\PYG{k+kn}{import} \PYG{n+nn}{matplotlib.pyplot} \PYG{k}{as} \PYG{n+nn}{plt}
\PYG{k+kn}{from} \PYG{n+nn}{matplotlib} \PYG{k+kn}{import} \PYG{n}{rc}
\PYG{n}{rc}\PYG{p}{(}\PYG{l+s+s1}{\PYGZsq{}font\PYGZsq{}}\PYG{p}{,}\PYG{o}{**}\PYG{p}{\PYGZob{}}\PYG{l+s+s1}{\PYGZsq{}family\PYGZsq{}}\PYG{p}{:}\PYG{l+s+s1}{\PYGZsq{}sans\PYGZhy{}serif\PYGZsq{}}\PYG{p}{,}\PYG{l+s+s1}{\PYGZsq{}sans\PYGZhy{}serif\PYGZsq{}}\PYG{p}{:[}\PYG{l+s+s1}{\PYGZsq{}Helvetica\PYGZsq{}}\PYG{p}{]\PYGZcb{})}
\PYG{n}{rc}\PYG{p}{(}\PYG{l+s+s1}{\PYGZsq{}text\PYGZsq{}}\PYG{p}{,} \PYG{n}{usetex}\PYG{o}{=}\PYG{k+kc}{True}\PYG{p}{)}

\PYG{n}{A}\PYG{o}{=}\PYG{l+m+mi}{100}
\PYG{n}{B}\PYG{o}{=}\PYG{l+m+mi}{50}
\PYG{n}{oms}\PYG{o}{=} \PYG{l+m+mi}{49}
\PYG{n}{M} \PYG{o}{=} \PYG{l+m+mi}{1000}
\PYG{n}{dk} \PYG{o}{=} \PYG{l+m+mi}{2}\PYG{o}{*}\PYG{n}{np}\PYG{o}{.}\PYG{n}{pi}\PYG{o}{*}\PYG{n}{M}\PYG{o}{**}\PYG{p}{(}\PYG{o}{\PYGZhy{}}\PYG{l+m+mi}{1}\PYG{p}{)}
\PYG{n}{k} \PYG{o}{=} \PYG{n}{np}\PYG{o}{.}\PYG{n}{arange}\PYG{p}{(}\PYG{o}{\PYGZhy{}}\PYG{n}{M}\PYG{o}{/}\PYG{l+m+mi}{2}\PYG{p}{,}\PYG{n}{M}\PYG{o}{/}\PYG{l+m+mi}{2}\PYG{o}{+}\PYG{l+m+mi}{1}\PYG{p}{)}\PYG{o}{*}\PYG{n}{dk}
\PYG{n}{b} \PYG{o}{=} \PYG{n}{np}\PYG{o}{.}\PYG{n}{array}\PYG{p}{([[}\PYG{l+m+mi}{0}\PYG{p}{,}\PYG{l+m+mi}{1}\PYG{p}{],[}\PYG{l+m+mi}{0}\PYG{p}{,}\PYG{l+m+mi}{0}\PYG{p}{]])}
\PYG{n}{bc} \PYG{o}{=} \PYG{n}{np}\PYG{o}{.}\PYG{n}{array}\PYG{p}{([[}\PYG{l+m+mi}{0}\PYG{p}{,}\PYG{l+m+mi}{0}\PYG{p}{],[}\PYG{l+m+mi}{1}\PYG{p}{,}\PYG{l+m+mi}{0}\PYG{p}{]])}
\PYG{n}{dt} \PYG{o}{=} \PYG{l+m+mf}{0.001}
\PYG{n}{omk} \PYG{o}{=} \PYG{n}{A} \PYG{o}{+} \PYG{n}{B}\PYG{o}{*}\PYG{n}{np}\PYG{o}{.}\PYG{n}{cos}\PYG{p}{(}\PYG{o}{\PYGZhy{}}\PYG{n}{np}\PYG{o}{.}\PYG{n}{pi} \PYG{o}{+} \PYG{n}{k}\PYG{p}{)}
\PYG{c+c1}{\PYGZsh{} I\PYGZsq{}m attempting to avoid working with \PYGZgt{}2\PYGZca{}1000 dimensional matrices (kroneckering}
\PYG{c+c1}{\PYGZsh{} together a 1000 ladder operators), but this approach doesn\PYGZsq{}t seem to work}
\PYG{n}{hmat0} \PYG{o}{=} \PYG{n}{np}\PYG{o}{.}\PYG{n}{zeros}\PYG{p}{((}\PYG{n}{M}\PYG{o}{+}\PYG{l+m+mi}{1}\PYG{p}{,}\PYG{n}{M}\PYG{o}{+}\PYG{l+m+mi}{1}\PYG{p}{))}
\PYG{k}{for} \PYG{n}{i} \PYG{o+ow}{in} \PYG{n+nb}{range}\PYG{p}{(}\PYG{l+m+mi}{1}\PYG{p}{,}\PYG{n}{M}\PYG{o}{+}\PYG{l+m+mi}{1}\PYG{p}{):}
    \PYG{n}{hmat0}\PYG{p}{[}\PYG{n}{i}\PYG{p}{,}\PYG{n}{i}\PYG{p}{]} \PYG{o}{=} \PYG{n}{omk}\PYG{p}{[}\PYG{n}{i}\PYG{o}{\PYGZhy{}}\PYG{l+m+mi}{1}\PYG{p}{]}
\PYG{n}{hmat1} \PYG{o}{=} \PYG{n}{np}\PYG{o}{.}\PYG{n}{zeros}\PYG{p}{((}\PYG{n}{M}\PYG{o}{+}\PYG{l+m+mi}{1}\PYG{p}{,}\PYG{n}{M}\PYG{o}{+}\PYG{l+m+mi}{1}\PYG{p}{))}
\PYG{k}{for} \PYG{n}{i} \PYG{o+ow}{in} \PYG{n+nb}{range}\PYG{p}{(}\PYG{l+m+mi}{1}\PYG{p}{,}\PYG{n}{M}\PYG{o}{+}\PYG{l+m+mi}{1}\PYG{p}{):}
    \PYG{n}{hmat1}\PYG{p}{[}\PYG{n}{i}\PYG{p}{,}\PYG{l+m+mi}{0}\PYG{p}{]} \PYG{o}{=} \PYG{l+m+mi}{1}
    \PYG{n}{hmat1}\PYG{p}{[}\PYG{l+m+mi}{0}\PYG{p}{,}\PYG{n}{i}\PYG{p}{]} \PYG{o}{=} \PYG{l+m+mi}{1}
\PYG{n}{hmat2} \PYG{o}{=} \PYG{n}{oms}\PYG{o}{*}\PYG{n}{np}\PYG{o}{.}\PYG{n}{zeros}\PYG{p}{((}\PYG{n}{M}\PYG{o}{+}\PYG{l+m+mi}{1}\PYG{p}{,}\PYG{n}{M}\PYG{o}{+}\PYG{l+m+mi}{1}\PYG{p}{))}
\PYG{n}{hmat2}\PYG{p}{[}\PYG{l+m+mi}{0}\PYG{p}{,}\PYG{l+m+mi}{0}\PYG{p}{]} \PYG{o}{=} \PYG{n}{oms}
\PYG{n}{hmat} \PYG{o}{=} \PYG{n}{hmat0} \PYG{o}{+} \PYG{n}{hmat1} \PYG{o}{+} \PYG{n}{hmat2}
\PYG{c+c1}{\PYGZsh{}w, v = la.eigh(hmat)}
\PYG{c+c1}{\PYGZsh{}print(np.matmul(v.T,v.conj()))}
\PYG{c+c1}{\PYGZsh{}eighmat = np.diag(w)}
\PYG{n}{state} \PYG{o}{=} \PYG{n}{np}\PYG{o}{.}\PYG{n}{zeros}\PYG{p}{(}\PYG{n}{M}\PYG{o}{+}\PYG{l+m+mi}{1}\PYG{p}{)}
\PYG{n}{ReAt} \PYG{o}{=} \PYG{n}{np}\PYG{o}{.}\PYG{n}{ones}\PYG{p}{(}\PYG{l+m+mi}{1000}\PYG{p}{)}
\PYG{n}{ImAt} \PYG{o}{=} \PYG{n}{np}\PYG{o}{.}\PYG{n}{zeros}\PYG{p}{(}\PYG{l+m+mi}{1000}\PYG{p}{)}
\PYG{n}{state}\PYG{p}{[}\PYG{l+m+mi}{0}\PYG{p}{]} \PYG{o}{=} \PYG{l+m+mi}{1}
\PYG{n}{nt} \PYG{o}{=} \PYG{n}{np}\PYG{o}{.}\PYG{n}{ones}\PYG{p}{(}\PYG{l+m+mi}{1000}\PYG{p}{)}
\PYG{n}{dUt} \PYG{o}{=} \PYG{n}{la}\PYG{o}{.}\PYG{n}{expm}\PYG{p}{(}\PYG{o}{\PYGZhy{}}\PYG{l+m+mi}{1}\PYG{n}{j}\PYG{o}{*}\PYG{n}{dt}\PYG{o}{*}\PYG{n}{hmat}\PYG{p}{)}
\PYG{k}{for} \PYG{n}{i} \PYG{o+ow}{in} \PYG{n+nb}{range}\PYG{p}{(}\PYG{l+m+mi}{1}\PYG{p}{,}\PYG{l+m+mi}{1000}\PYG{p}{):}
    \PYG{n}{state} \PYG{o}{=} \PYG{n}{np}\PYG{o}{.}\PYG{n}{matmul}\PYG{p}{(}\PYG{n}{dUt}\PYG{p}{,}\PYG{n}{state}\PYG{p}{)}
    \PYG{n}{ReAt}\PYG{p}{[}\PYG{n}{i}\PYG{p}{]} \PYG{o}{=} \PYG{n}{state}\PYG{p}{[}\PYG{n}{i}\PYG{p}{]}\PYG{o}{.}\PYG{n}{real}
    \PYG{n}{ImAt}\PYG{p}{[}\PYG{n}{i}\PYG{p}{]} \PYG{o}{=} \PYG{n}{state}\PYG{p}{[}\PYG{n}{i}\PYG{p}{]}\PYG{o}{.}\PYG{n}{imag}
    \PYG{n}{nt}\PYG{p}{[}\PYG{n}{i}\PYG{p}{]} \PYG{o}{=} \PYG{n+nb}{abs}\PYG{p}{(}\PYG{n}{state}\PYG{p}{[}\PYG{l+m+mi}{0}\PYG{p}{])}\PYG{o}{**}\PYG{l+m+mi}{2}
\PYG{n}{fig}\PYG{p}{,} \PYG{n}{ax} \PYG{o}{=} \PYG{n}{plt}\PYG{o}{.}\PYG{n}{subplots}\PYG{p}{()}
\PYG{n}{t} \PYG{o}{=} \PYG{n}{np}\PYG{o}{.}\PYG{n}{arange}\PYG{p}{(}\PYG{l+m+mi}{1000}\PYG{p}{)}\PYG{o}{*}\PYG{n}{dt}
\PYG{n}{ax}\PYG{o}{.}\PYG{n}{plot}\PYG{p}{(}\PYG{n}{t}\PYG{p}{,} \PYG{n}{nt}\PYG{p}{)}
\PYG{n}{ax}\PYG{o}{.}\PYG{n}{set}\PYG{p}{(}\PYG{n}{xlabel}\PYG{o}{=}\PYG{l+s+s2}{\PYGZdq{}\PYGZdl{}t\PYGZdl{}\PYGZdq{}}\PYG{p}{,} \PYG{n}{ylabel} \PYG{o}{=} \PYG{l+s+s2}{\PYGZdq{}\PYGZdl{}n\PYGZus{}a(t)\PYGZdl{}\PYGZdq{}}\PYG{p}{)}
\PYG{n}{ax}\PYG{o}{.}\PYG{n}{grid}\PYG{p}{()}

\PYG{n}{fig}\PYG{o}{.}\PYG{n}{savefig}\PYG{p}{(}\PYG{l+s+s2}{\PYGZdq{}singleexc49.png\PYGZdq{}}\PYG{p}{)}

\PYG{n}{fig}\PYG{p}{,} \PYG{n}{ax} \PYG{o}{=} \PYG{n}{plt}\PYG{o}{.}\PYG{n}{subplots}\PYG{p}{()}
\PYG{n}{ax}\PYG{o}{.}\PYG{n}{plot}\PYG{p}{(}\PYG{n}{t}\PYG{p}{,} \PYG{n}{ReAt}\PYG{p}{)}
\PYG{n}{ax}\PYG{o}{.}\PYG{n}{set}\PYG{p}{(}\PYG{n}{xlabel}\PYG{o}{=}\PYG{l+s+s2}{\PYGZdq{}\PYGZdl{}t\PYGZdl{}\PYGZdq{}}\PYG{p}{,} \PYG{n}{ylabel} \PYG{o}{=} \PYG{l+s+s2}{\PYGZdq{}\PYGZdl{}}\PYG{l+s+se}{\PYGZbs{}\PYGZbs{}}\PYG{l+s+s2}{Re(A(t))\PYGZdl{}\PYGZdq{}}\PYG{p}{)}
\PYG{n}{ax}\PYG{o}{.}\PYG{n}{grid}\PYG{p}{()}

\PYG{n}{fig}\PYG{o}{.}\PYG{n}{savefig}\PYG{p}{(}\PYG{l+s+s2}{\PYGZdq{}reat.png\PYGZdq{}}\PYG{p}{)}

\PYG{n}{fig}\PYG{p}{,} \PYG{n}{ax} \PYG{o}{=} \PYG{n}{plt}\PYG{o}{.}\PYG{n}{subplots}\PYG{p}{()}
\PYG{n}{ax}\PYG{o}{.}\PYG{n}{plot}\PYG{p}{(}\PYG{n}{t}\PYG{p}{,} \PYG{n}{ImAt}\PYG{p}{)}
\PYG{n}{ax}\PYG{o}{.}\PYG{n}{set}\PYG{p}{(}\PYG{n}{xlabel}\PYG{o}{=}\PYG{l+s+s2}{\PYGZdq{}\PYGZdl{}t\PYGZdl{}\PYGZdq{}}\PYG{p}{,} \PYG{n}{ylabel} \PYG{o}{=} \PYG{l+s+s2}{\PYGZdq{}\PYGZdl{}}\PYG{l+s+se}{\PYGZbs{}\PYGZbs{}}\PYG{l+s+s2}{Im(A(t))\PYGZdl{}\PYGZdq{}}\PYG{p}{)}
\PYG{n}{ax}\PYG{o}{.}\PYG{n}{grid}\PYG{p}{()}

\PYG{n}{fig}\PYG{o}{.}\PYG{n}{savefig}\PYG{p}{(}\PYG{l+s+s2}{\PYGZdq{}imat.png\PYGZdq{}}\PYG{p}{)}
\end{Verbatim}
