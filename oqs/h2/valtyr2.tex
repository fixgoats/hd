%&heimhaus
\begin{document}
  \centerline{\bf \huge Open Quantum Systems}
  \centerline{V. Kári Daníelsson, 2019-10-29}
  \centerline{\bf \Large Exercise sheet 2}
  
  \vspace{1cm}

  \begin{itemize}
    \item[1.] The time derivative of the UDM is
      \begin{align*}
        \dot{\phi}[\rho_S] = \sum_l \frac{dE_l}{dt} (t) \rho_s(0)E^\dagger_l(t) +
        E_l(t) \rho_s(0) E^\dagger_l (t)
      \end{align*}
      So the expression for \(\dot{\phi}[\phi^{-1}[\rho_s(t)]]\) is
      \begin{align*}
        \sum_{l,m} \frac{dE_l}{dt} (t)F_m(t)\rho_s(t)Q_m(t)E^\dagger_l(t) +
        E_l(t)F_m(t) \rho_s(t)Q_m(t) \frac{dE^\dagger_l}{dt} (t)
      \end{align*}
      We can put this in the form
      \begin{align*}
        \sum_k A_k(t)\rho_s(t)B_k(t)
      \end{align*}
      By defining \(k = (j,l,m)\) where \(j\in\{1,2\}\) and \(A_{1lm} =
      \frac{dE_l}{dt} (t)F_m(t)\), \(A_{2lm} = E_l(t)F_m(t)\), \(B_{1lm} =
      Q^\dagger_m(t)E_l(t)\) and \(B_{2lm} = Q^\dagger_m(t) \frac{dE_l}{dt} (t)\)

    \item[2.] Using the Gell-Mann basis we can write the operators \(A_k(t)\),
      \(B_k(t)\) as
      \begin{align*}
        A_k(t) = \sum_i G_i \tr_S\{G_iA_k(t)\} \\
        B_k(t) = \sum_j G_j \tr_S\{G_jB_k(t)\}
      \end{align*}
      Since \(A_k(t)\) and \(B_k(t)\) are operators in the open system, the
      traces are simply scalars (or if we extend to the whole system, scalar
      multiples of the identity operator) so we can lump the traces into
      \(c_{ij}\).
      \begin{align*}
        \frac{d\rho_s(t)}{dt} = \sum_{i,j=0}^{N-1}\left(\sum_k
        \tr_S\{G_iA_k(t)\}\tr_S\{G_jB^\dagger_k(t)\}\right) G_i\rho_s(t)G_j
      \end{align*}
      Thus
      \begin{align*}
        c_{ij} = \sum_k \tr_S\{G_iA_k(t)\}\tr_S\{G_jB_k^\dagger(t)\}
      \end{align*}
      Since \(\dot{\rho_s}\), \(\rho_s\) and the Gell-Mann matrices are hermitian, the elements must satisfy \(c_{ij} =
      c_{ji}^*\)

    \item[3.] Since \(G_0 = \mathbbm{1}_S/\sqrt{d}\), for \(i=0\) the terms are
      \begin{align*}
        L = \frac{\sum_k \tr_S\{A_k(t)\}\tr_S\{B_k^\dagger(t)\}}{d^2}\rho_s(t) +
        \sum_{j=1}^{N-1} \sum_k
        \frac{\tr_S\{A_k(t)\}\tr_S\{G_jB^\dagger_k(t)\}}{d} \rho_s(t)G_j
      \end{align*}
      And similarly for \(j = 0\)
      \begin{align*}
        M = \frac{\sum_k \tr_S\{A_k(t)\}\tr_S\{B_k^\dagger(t)\}}{d^2}\rho_s(t) + \sum_{i=1}^{N-1}\sum_k \frac{\tr_S\{G_iA_k(t)\}\tr_S\{B^\dagger_k(t)\}}{d} G_i\rho_s(t) 
      \end{align*}
      The operator \(C\) has the form
      \begin{align*}
        C = \frac{\sum_k \tr_S\{A_k(t)\}\tr_S\{B_k^\dagger(t)\}}{d^2} +
        \sum_{i=1}^{N-1}\sum_k \frac{\tr_S\{G_iA_k(t)\}\tr_S\{B_k^\dagger(t)\}}{d} G_i
      \end{align*}
      And its complex conjugate is
      \begin{align*}
        C^\dagger &= \frac{\sum_k\tr_S\{A_k(t)\}\tr_S\{B_k^\dagger(t)\}}{d^2} +
        \sum_{i=1}^{N-1}\sum_k \frac{\tr_S\{A_k(t)\}\tr_S\{G_iB_k^\dagger(t)\}}{d} G_i
      \end{align*}
      Then
      \begin{align*}
        L = \rho_s(t)C^\dagger \\
        M = C\rho_s(t)
      \end{align*}
      So the formula is
      \begin{align*}
        \frac{d\rho_s}{dt} (t) = C\rho_s(t) + \rho_s(t)C^\dagger +
        \sum_{i,j=1}^{N-1} c_{ij}G_i\rho_s(t)G_j - \frac{c_{00}}{d}\rho_s(t)
      \end{align*}
      Since the trace must be zero we obtain
      \begin{align*}
        \tr{C\rho_s(t)} + \tr{\rho_s(t)C^\dagger} + 
        \tr{\sum_{i,j=1}^{N-1}c_{ij}G_i\rho_s(t)G_j - \frac{c_{00}}{d}\rho_s(t)} = 0
      \end{align*}
      Since the trace is invariant under cyclic permutation of operators we obtain
      \begin{align*}
        \tr{(C + C^\dagger)\rho_s(t)} +
        \tr{ \left(\sum_{i,j=1}^{N-1}c_{ij}G_jG_i -
        \frac{c_{00}}{d}\right)\rho_s(t)} = 0
      \end{align*}
      So we obtain
      \begin{align*}
        (C + C^\dagger) = \frac{c_{00}}{d} - \sum_{i,j=1}^{N-1}c_{ij}G_jG_i 
      \end{align*}

    \item[4.] We can rewrite \(C\) as \( \frac{1}{2} (C + C^\dagger) + \frac{1}{2} (C -
      C^\dagger)\) and \(C^\dagger\) as \(\frac{1}{2}(C + C^\dagger) -
      \frac{1}{2}(C - C^\dagger)\). Using the identity for \(C + C^\dagger\)
      obtained above we obtain
      \begin{align*}
        \frac{d\rho_s}{dt} (t) &= \frac{1}{2} (C - C^\dagger)\rho_s(t) - \frac{1}{2} 
        \rho_s(t)(C - C^\dagger) + \frac{c_{00}}{d} - \frac{c_{00}}{d}\\
                               &\qquad +\sum_{i,j=1}^{N-1} \left[c_{ij}G_i\rho_s(t)G_j
                                 - \frac{c_{ij}}{2}(G_jG_i\rho_s(k) +
                             \rho_s(k)G_jG_i)\right] \\
                               &= -i[H, \rho_s] +
                               \sum_{i,j=1}^{N-1}c_{ij}(G_iG_j - \frac{1}{2}
                               \{G_jG_i,\rho_s\})
      \end{align*}

    \item[5.] By applying property (9) we verify
      \begin{align*}
          \sum_{m=0}^{N-1}\tr_S\{G_mG_i\Lambda_t[G_m]G_j\} 
          &= \sum_{m=0}^{N-1}
          \sum^{N-1}_{n=0} G_nG_mG_i\left(\sum_k A_k G_m
          B_k^\dagger\right)G_jG_n \\
          &= \sum_k \tr_S \left\{ \sum^{N-1}_{n=0} G_n \tr_S{G_iA_k}B_k^\dagger
          G_jG_n\right\} \\
          &= \sum_k \tr_S \{\tr_S\{G_iA_k\} B_k^\dagger G_j\} \\
          &= \sum_k \tr_S\{G_iA_k\}\tr_S\{G_jB_k^\dagger\}
      \end{align*}

    \item[6.] We substitute \(c_{ij}(t) = \sum_{k=1}^{d^2-1}
      U_{ik}(t)\gamma_k(t) U^*_{jk}(t)\) into the equation:
      \begin{align*}
        \frac{d\rho_s}{dt} (t) 
        &= -i[H,\rho_s(t)] + \sum_{i,j=1}^{N-1} \sum_{k=1}^{d^2-1}
        U_{ik}(t)\gamma_k(t) U^*_{jk}(t) (G_iG_j - \frac{1}{2} \{G_jG_i, \rho_s(t)\}) \\
        &= \ldots + \sum_{k=1}^{d^2-1} \gamma_k(t) \sum_{i,j=1}^{d^2-1}
        U_{ik}(t)G_iG_jU^*_{jk}(t) - \frac{1}{2}\{U^*_{jk}(t)G_jG_iU_{ik}(t), \rho_s(t)\} 
      \end{align*}
      By defining \(L_k(t) = \sum_{i=1}^{d^2-1}U_ik(t) G_i\) we see that the equation is
      indeed of the form
      \begin{align*}
        \frac{d\rho_s}{dt} (t) = -i[H,\rho_s(t)] + \sum_{k=1}^{d^2-1}
        \gamma_k(t)\left(L_k(t)L^\dagger_k(t) - \frac{1}{2} \left\{L^\dagger_k(t)L_k(t),
        \rho_s(t)\right\}\right)
      \end{align*}
  \end{itemize}
\end{document}
