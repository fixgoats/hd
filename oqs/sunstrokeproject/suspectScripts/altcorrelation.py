import numpy as np
import scipy.linalg as la
import matplotlib.pyplot as plt
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

A=100
B=50
oms= 55
N = 2
M = 100
dim = M + N + 1
g = 0.1
dk = 2*np.pi*M**(-1)
k = np.arange(-M/2,M/2+1)*dk
b = np.array([[0,1],[0,0]])
bc = np.array([[0,0],[1,0]])
dt = 0.01
omk = A + B*np.cos(k)
hmat0 = np.zeros((dim,dim))
n0 = 0
n1 = 1
n = np.array([n0,n1])
for i in range(M):
    hmat0[N+1+i,N+i+1] = omk[i]
hmat1 = np.zeros((dim,dim)) + 0j
for j in range(N):
    for i in range(M):
        hmat1[j+1,N+i+1] = g*np.exp(1j*k[i]*n[j])
        hmat1[i+N+1,j+1] = g*np.exp(-1j*k[i]*n[j])
hmat2 = oms*np.zeros((dim,dim))
hmat2[1,1] = oms
hmat2[2,2] = oms
hmat = hmat0 + hmat1 + hmat2
couplingop0 = np.zeros((dim,dim)) + 0j
couplingop0[0,N+1:] = np.exp(1j*k*n0)
couplingop1 = np.zeros((dim,dim)) + 0j
couplingop1[0,N+1:] = np.exp(1j*k*n1)
#w, v = la.eigh(hmat)
#print(np.matmul(v.T,v.conj()))
#eighmat = np.diag(w)
corrmat = np.eye(dim)
corrmat[0:2,0:2] = np.array([[0,1],[1,0]])
state = la.expm(hmat)*np.
state[0,0] = 1
corr = np.zeros(1000) + 0j
dUt = la.expm(-1j*dt*hmat)
dUtdagger = dUt.conj()
for i in range(1000):
    corr[i] = np.trace(np.matmul(corrmat,state))
    state = np.matmul(dUt,np.matmul(state,dUtdagger))
recorr = corr.real
imcorr = corr.imag
fig, ax = plt.subplots()
t = np.arange(1000)*dt
ax.plot(t, recorr)
ax.set(xlabel="$t$", ylabel = "$C_{12}(t)$")
ax.grid()
figname = input("Figure file name")
fig.savefig(figname)
