import sys
import numpy as np
import scipy.linalg as la
import matplotlib.pyplot as plt
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

A = 100
B = 50
g = 0.1
M = 100
dk = 2*np.pi*M**(-1)
knum = np.arange(-M/2,M/2)*dk
b = np.array([[0,1],[0,0]])
bc = np.array([[0,0],[1,0]])
dt = 0.01
omk = A + B*np.cos(knum)
CEvac = np.zeros(1000) + 0j
for i in range(1000):
    CEvac[i] = g**2 * np.sum(np.exp(-1j*omk*i*dt + 1j*knum))

reCEvac = CEvac.real
t = np.arange(0,1000)*dt
fig, ax = plt.subplots()
ax.plot(t,reCEvac)
ax.set(xlabel="$t$", ylabel="$C_E^{vac}$")
ax.grid()

fig.savefig("Figures/ex1/CEvac10001.png")

thBelow = 1 + np.sum(np.exp(-omk))
thAbove1 = CEvac
thAbove2 = np.zeros(1000) + 0j
for i in range(1000):
    for j in range(M):
        thAbove2[i] = thAbove2[i] + g**2*np.exp(-0.1*omk[j])*(np.sum(np.exp(1j*knum-1j*omk*i*dt)) + np.sum(np.exp(1j*knum[j]-1j*omk[j]*i*dt)))
CEth =(thAbove1+thAbove2)*(thBelow)**(-1)
reCEth = CEth.real
fig, ax = plt.subplots()
ax.plot(t,reCEth)
ax.set(xlabel="$t$", ylabel="$C_E^{th}$")
ax.grid()
fig.savefig("Figures/ex1/CEth10001.png")
