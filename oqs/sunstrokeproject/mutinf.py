import sys
import argparse
import numpy as np
import scipy.linalg as la
import matplotlib.pyplot as plt
from matplotlib import rc


if not sys.warnoptions:
    import warnings
    warnings.simplefilter("ignore")
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

parser = argparse.ArgumentParser()
parser.add_argument(
        "fn", type=str, nargs='?', help="File containing partial traces"
        ) 
args = parser.parse_args()
mutualinf = np.zeros(1000)
altmutualinf = np.zeros(1000)

for i in range(1000):
    bois = np.loadtxt(args.fn, skiprows=3*i, max_rows=3, dtype=complex)
    # Reduced density op. for atom 1 with (1,0) excited, (0,1) unexcited
    ptrace1 = np.array(
            [[bois[0,0], bois[0,1]],
            [bois[1,0], bois[1,1] + bois[2,2]]]
            )
    # Reduced density op. for atom 2 with (2,0) unexcited, (0,1) excited
    ptrace2 = np.array(
            [[bois[0,0] + bois[2,2], bois[0,1]],
            [bois[1,0], bois[1,1]]]
            )
    lpt1 = np.nan_to_num(la.logm(ptrace1))
    lpt2 = np.nan_to_num(la.logm(ptrace2))
    lbois = np.nan_to_num(la.logm(bois))
    mutualinf[i] = -np.trace(np.matmul(ptrace1,lpt1)) - np.trace(np.matmul(ptrace2,lpt2)) + np.trace(np.matmul(bois,lbois))

fig, ax = plt.subplots()
dt = 0.01
t = np.arange(1000)*dt
I1 = ax.plot(t, mutualinf)
ax.set(xlabel="$t$", ylabel = "$I(1:2)$")
ax.grid()
#ax.legend()
figname = "entmutualinf" + args.fn[10:len(args.fn) - 4]
#figname = "mutualinf" + args.fn[::-1][4:19][::-1]
#if "ent" in args.fn:
#    figname = "ent" + figname
fig.savefig("Figures/ex5/" + figname + ".pdf")
