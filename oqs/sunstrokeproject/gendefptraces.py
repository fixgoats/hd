import argparse
import numpy as np
import scipy.sparse as sp
import scipy.linalg as la

parser = argparse.ArgumentParser()
parser.add_argument("M", type=int, nargs='?', help="Number of environment modes", default=100)
parser.add_argument("g", type=float, nargs='?', help="Coupling coefficient", default=0.1)
parser.add_argument("oms", type=int, nargs='?', help="System characteristic energy",default=49)
args = parser.parse_args()
M, g, oms, n2, n1 = args.M, args.g, args.oms
dn = 0.1*np.random.randn(200)
n1 = 1 + dn[::2]
n2 = 0 + dn[1::2]
n = np.concatenate((n1,n2))
name = "M" + str(M) + "g" + str(g) + "oms" + str(oms) #+ "d" + str(n2-n1)
A=100
B=50
N = 2
dim = M + N# + 1
dk = 2*np.pi*M**(-1)
k = np.arange(-M/2,M/2+1)*dk
dt = 0.01
omk = A + B*np.cos(k)
hmat0 = sp.lil((dim,dim)) + 0j
for i in range(M):
    hmat0[N+i,N+i] = omk[i]
hmat1 = np.zeros((dim,dim,100)) + 0j
for m in range(100):
    for j in range(N):
        for i in range(M):
            hmat1[j,N+i] = g*np.exp(1j*k[i]*n[m + 100*j])
            hmat1[i+N,j] = g*np.exp(-1j*k[i]*n[m + 100*j])
hmat2 = np.zeros((dim,dim)) + 0j
hmat2[0,0] = oms
hmat2[1,1] = oms
hmat = hmat0 + hmat1 + hmat2
state = np.zeros((dim,dim)) + 0j
state[0,0] = 1
dUt = la.expm(-1j*dt*hmat)
dUtdagger = dUt.conj()
f = open("ptraces"+name+".out", "ab")
for i in range(1000):
    for m in range(100):
        ptrace = np.zeros((3,3)) + 0j
        ptrace[0:2,0:2] = state[0:2,0:2]
        ptrace[2,2] = np.trace(state[2:,2:])
        np.savetxt(f,ptrace)
        state = np.matmul(dUt,np.matmul(state,dUtdagger))
f.close()
