import sys
import argparse
import numpy as np
import scipy.linalg as la
import matplotlib.pyplot as plt
from matplotlib import rc


if not sys.warnoptions:
    import warnings
    warnings.simplefilter("ignore")
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

parser = argparse.ArgumentParser()
parser.add_argument(
        "fn", type=str, nargs='?', help="File containing partial traces"
        ) 
args = parser.parse_args()
entropy = np.zeros(1000)

for i in range(1000):
    bois = np.loadtxt(args.fn, skiprows=3*i, max_rows=3, dtype=complex)
    ptrace = np.array(
            [[bois[0,0], bois[0,1]],
            [bois[1,0], bois[1,1] + bois[2,2]]]
            )
    lpt = np.nan_to_num(la.logm(ptrace))
    entropy[i] = -np.trace(np.matmul(ptrace,lpt))

fig, ax = plt.subplots()
dt = 0.01
t = np.arange(1000)*dt
ax.plot(t, entropy)
ax.set(xlabel="$t$", ylabel = "$S(\\rho^{12})$")
ax.grid()
figname = "entropy" + args.fn[7:len(args.fn)-4]
fig.savefig("Figures/ex3/" + figname + ".pdf")
