import sympy as sp

xi1, xi2 = sp.symbols('xi1 xi2')
x = 2*xi1/(xi1**2 + xi2**2 + 1)
y = 2*xi2/(xi1**2 + xi2**2 + 1)
z = (xi1**2 + xi2**2 - 1)/(xi1**2 + xi2**2 + 1)
v = sp.Matrix([x,y,z])
sp.init_printing()
g11 = sp.simplify(sp.diff(v.T,xi1)*sp.diff(v, xi1))
g12 = sp.simplify(sp.diff(v.T,xi1)*sp.diff(v, xi2))
g21 = sp.simplify(sp.diff(v.T,xi2)*sp.diff(v, xi1))
g22 = sp.simplify(sp.diff(v.T,xi2)*sp.diff(v, xi2))
g = sp.Matrix([[g11, g12], [g21, g22]])
print(sp.latex(g))
